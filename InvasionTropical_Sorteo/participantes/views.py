from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.contrib.auth.models import User

from .models import Participantes
from .forms import ParticipantesForm
# Create your views here.

def validar_cupon(cupon):
	cupon_existente = Participantes.objects.filter(nro_cupon=cupon)
	if cupon_existente == None:
		return False
	else:
		return True	

@login_required
def registrarParticipantes(request):
	if(request.method=='POST'):
		formparticipantes = ParticipantesForm(request.POST)
		if formparticipantes.is_valid():
			cupon = formparticipantes.cleaned_data['nro_cupon']
			validar_cupon(cupon)
			cupon_registrado = validar_cupon(cupon)
			if cupon_registrado == True:			
				formparticipantes = ParticipantesForm()
				contexto = {
					'formulario': formparticipantes,
					'mensaje':'Atencion!, el cupon que quiste registrar no esta disponibe',
				}
				return render(request,'formulario/registro.html',contexto)
			else:				
				formularioparticipantes = formparticipantes.save(commit=False)
				formularioparticipantes.creacion_usuario = request.user
				formularioparticipantes.modificacion_usuario = request.user
				formularioparticipantes.save()
				return redirect(to='app_usuarios:Home')
	formularios={
		'formularioParticipante':ParticipantesForm(),

	}
	return render(request,'formulario/registro.html',formularios)

@login_required
def lista(request):
	usuario = request.user.is_staff
	print(usuario)
	if usuario == True:
		lista = Participantes.objects.all()
		return render(request,'lista/listadeparticipantes.html',{'listado':lista,'staff':True})
	else:
		lista = Participantes.objects.filter(creacion_usuario=usuario)	
		return render(request,'lista/listadeparticipantes.html',{'listado':lista,'staff':False})