from django.forms import ModelForm,widgets,TextInput,NumberInput,HiddenInput
from django.contrib.auth.models import User

from .models import Participantes

class ParticipantesForm(ModelForm):
	class Meta:
		model = Participantes
		fields = (
			'nombre',
			'apellido',
			'dni',
			'nro_telefono',
			'direccion',
			'nro_cupon'
			)
		excluds = (
        		'creacion_usuario',
        		'modificacion_usuario'
        	)

		widgets={
			'nombre': TextInput(
				attrs = {
					
					'autocomplete':'off',
					'placeholder':'Ingrese el nombre'
                }
            ),
			'apellido': TextInput(
				attrs = {
					'autocomplete':'off',
					'placeholder':'Ingrese el apellido'
				}
			),
			'dni': NumberInput(
				attrs = {
					'autocomplete':'off',
					'placeholder':'Ingrese el Nro de DNI'
				}

			),
			'nro_telefono': NumberInput(
				attrs = {
					'autocomplete':'off',
					'placeholder':'Ingrese el numero de telefono o celular'

				}	
			),
			'direccion': TextInput(
				attrs = {
					'autocomplete':'off',
					'placeholder':'Ingrese la direccion'

				}	
			),
			'nro_cupon': NumberInput(
				attrs = {
					'autocomplete':'off',
					'placeholder':'Ingrese el numero del cupon',
					'maxlength':4,
					'max':'1000'
				}
			)}

