# Generated by Django 3.2.9 on 2021-11-27 00:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('participantes', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='participantes',
            name='nro_cupon',
            field=models.IntegerField(default=1, verbose_name='Numero de cupon'),
            preserve_default=False,
        ),
    ]
