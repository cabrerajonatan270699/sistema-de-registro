from django.db import models
from base.models import CamposComunes
# Create your models here.
class Participantes(CamposComunes):
	nombre = models.CharField(
		max_length=40,
		verbose_name='Nombre'
		)
	apellido = models.CharField(
		max_length=40,
		verbose_name='Apellido')
	dni = models.IntegerField(
		verbose_name='Numero de DNI'
		)
	nro_telefono = models.IntegerField(
		verbose_name='Numero de telefono'
		)
	direccion = models.CharField(
		max_length=100,
		verbose_name='Direccion'
		)
	nro_cupon = models.IntegerField(
		null = False,
		verbose_name='Numero de cupon'
		)

	def __str__(self):
		return f'{self.nombre} {self.apellido} {self.dni} {self.nro_cupon} {self.creacion_usuario}'
