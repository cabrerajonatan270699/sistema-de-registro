from django.urls import path
from .views import registrarParticipantes,lista
# from participantes import views
app_name= 'app_participantes'
urlpatterns = [
    path('registro/',registrarParticipantes,name="registro" ),
    path('listadodeventas/',lista,name='lista')

]
