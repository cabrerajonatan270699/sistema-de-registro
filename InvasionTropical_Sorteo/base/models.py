from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class CamposComunes(models.Model):
    creacion_fecha = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Fecha de creación',
    )
    creacion_usuario = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="%(app_label)s_%(class)s_related",
        related_query_name="%(app_label)s_%(class)ss",
        verbose_name='Usuario de creación',
    )
    modificacion_fecha = models.DateTimeField(
        auto_now=True,
        verbose_name='Fecha de modificación',
    )
    modificacion_usuario = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        verbose_name='Usuario de  modificación',
    )

    def __str__(self):
        return f'{self.creacion_fecha}'

    