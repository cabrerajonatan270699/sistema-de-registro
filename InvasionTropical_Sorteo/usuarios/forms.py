from django.contrib.auth import get_user_model
from django import forms

User = get_user_model()

class RegisterForm(forms.Form):
	username = forms.CharField(
		label='Nombre de usuario',
	)
	password1 = forms.CharField(
		label='Contraseña',
		widget=forms.PasswordInput(
				attrs={
					"id": "user-password"
				}
			)
		)
	password2 = forms.CharField(
		label='Confirmar contraseña',
		widget=forms.PasswordInput(
				attrs={
					"id": "user-confirm-password"
				}
			)
		)

	def clean_username(self):
		username = self.cleaned_data.get("username")
		qs = User.objects.filter(username__iexact=username)
		if qs.exists():
			raise forms.ValidationError("Este usuario ya existe, por favor usa otro")
		return username

	def clean_password(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 != password2:
			raise forms.ValidationError("Las contraseñas no coinciden")
		return password1