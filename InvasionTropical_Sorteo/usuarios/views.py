from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.admin.views.decorators import staff_member_required
# Create your views here.
from participantes.models import Participantes
from .forms import RegisterForm

def bienvenida(request):
    return render(request,'home2.html')

@login_required
def home(request):
    ultimo_cupon_vendido = Participantes.objects.last()
    usuario = request.user
    usuario_staff = request.user.is_staff
    listado_cupones_vendidos = Participantes.objects.filter(creacion_usuario=usuario).order_by('-creacion_usuario')[:10]
    cantidad_cupones_vendidos = Participantes.objects.all().count()
    if usuario_staff == True:

        return render(request, 'home.html', {'cupon': ultimo_cupon_vendido, 'lista_cupon': listado_cupones_vendidos,
                                             'cupones_vendidos': cantidad_cupones_vendidos, 'staff': True})
    else:
        return render(request, 'home.html', {'cupon': ultimo_cupon_vendido, 'lista_cupon': listado_cupones_vendidos,
                                             'cupones_vendidos': cantidad_cupones_vendidos, 'staff': False})


@login_required
@staff_member_required
def register_view(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password1")
            password2 = form.cleaned_data.get("password2")
            try:
                user = User.objects.create_user(username=username,password=password)
            except:
                user = None
            if user != None:
                return redirect(to="app_usuarios:Home")
            else:
                request.session['register_error'] = 1  # 1 == True
    form = RegisterForm()
    return render(request, "registration/agregar_usuario.html", {'form': form})

def salir(request):
    logout(request)
    return redirect(to='app_usuarios:Home')
