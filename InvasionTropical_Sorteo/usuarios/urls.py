from django.urls import path

from .views import bienvenida, home,register_view,salir

app_name='app_usuarios'
urlpatterns = [
    path('',bienvenida,name='bienvenida'),
    path('panel_de_control/',home,name='Home'),
    path('añadir_usuario/',register_view,name='usuario_nuevo'),
    path('salir/',salir, name='salir'),
]